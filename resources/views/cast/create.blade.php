@extends('layout.master')
@section('judul')
Halaman add cast
@endsection

@section('content')
    <h3>Sign Up Form</h3>
    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
            <label>Nama Cast</label>
            <input type="text" name="nama" class="form-control">
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror  
        </div>
        <div class="form-group">
            <label>Umur</label>
            <input type="text" name="umur" class="form-control">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror 
        <div class="form-group">
            <label>Bio</label>
            <textarea name="bio" class="form-control" cols="30" rows="10"></textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror 
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
@endsection