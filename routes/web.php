<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@dashboard'); 
Route::get('/regsiter','AuthController@register');
Route::post('/welcome','AuthController@welcome'); 

Route::get('table', function(){
    return view('halaman.table');
});

Route::get('/data-table', function(){
    return view('halaman.data-table');
});

//CRUD Cast
//create form input pemain baru
Route::get('/cast/create','CastController@create');
//simpan data pemain baru ke database
Route::post('/cast','CastController@store');

//read data
//menampilkan semua data di tabel cast
Route::get('/cast','CastController@index');

//menampilakan detail kategori
Route::get('/cast/{cast_id}','CastController@show');

//update
//form eidt data cast
Route::get('/cast/{cast_id}/edit','CastController@edit');
//update data berdasarkan id
Route::put('/cast/{cast_id}','CastController@update');

//delete
Route::delete('/cast/{cast_id}','CastController@destroy');
